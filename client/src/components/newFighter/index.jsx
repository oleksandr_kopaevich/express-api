import { TextField } from "material-ui";
import { createFighter } from "../../services/domainRequest/fightersRequest";
import React, { useState } from "react";
import { Button } from "@material-ui/core";
import "./newFighter.css";

export default function NewFighter({ onCreated }) {
  const [name, setName] = useState();
  const [power, setPower] = useState();
  const [defense, setDefense] = useState();
  const [health, setHealth] = useState();
  const [errors, setErrors] = useState({});
  const [successMsg, setSuccessMsg] = useState();

  const onNameChange = (event) => {
    setName(event.target.value);
  };

  const onPowerChange = (event) => {
    const value =
      event.target.value || event.target.value === 0
        ? Number(event.target.value)
        : null;
    setPower(value);
  };

  const onDefenseChange = (event) => {
    const value =
      event.target.value || event.target.value === 0
        ? Number(event.target.value)
        : null;
    setDefense(value);
  };

  const onHealthChange = (event) => {
    const value =
      event.target.value || event.target.value === 0
        ? Number(event.target.value)
        : null;
    setHealth(value);
  };

  const onSubmit = async () => {
    const data = await createFighter({ name, power, health, defense });
    setSuccessMsg(null);

    if (data && !data.error) {
      onCreated(data);
      setErrors({});
      const { name, health, power, defense } = data;
      setSuccessMsg(
        `${name} was successfully created with 🖤 ${health}HP, 💪 ${power} and 🛡️ ${defense}`
      );
    }

    if (data && data.error) {
      setErrors(data.errors);
    }
  };

  return (
    <div id="new-fighter">
      <div>New Fighter</div>
      <div class="new-fighter-wrap">
        <div>
          <TextField
            onChange={onNameChange}
            id="standard-basic"
            label="Standard"
            placeholder="Name"
          />
          {errors.name && <div className="form-error">{errors.name}</div>}
        </div>
        <div>
          <TextField
            onChange={onHealthChange}
            id="standard-basic"
            label="Standard"
            placeholder="Health"
            type="number"
          />
          {errors.health && <div className="form-error">{errors.health}</div>}
        </div>
        <div>
          <TextField
            onChange={onPowerChange}
            id="standard-basic"
            label="Standard"
            placeholder="Power"
            type="number"
          />
          {errors.power && <div className="form-error">{errors.power}</div>}
        </div>
        <div>
          <TextField
            onChange={onDefenseChange}
            id="standard-basic"
            label="Standard"
            placeholder="Defense"
            type="number"
          />
          {errors.defense && <div className="form-error">{errors.defense}</div>}
        </div>
        <Button onClick={onSubmit} variant="contained" color="primary">
          Create
        </Button>
      </div>
      {successMsg && <div class="success-message">{successMsg}</div>}
    </div>
  );
}

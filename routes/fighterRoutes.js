const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");
const { fighterValidationChain } = require("../models/fighter.validationChain");

const router = Router();

// TODO: Implement route controllers for fighter

router.get("/", (req, res) => {
  const allFighters = FighterService.getAllFighters();

  res.send(allFighters);
});

router.get("/:id", (req, res) => {
  const { id } = req.params;
  const fighter = FighterService.getFighterById(id);

  if (!fighter) {
    res.status(404).send({ error: true, message: `Fighter isn't found` });
  }

  res.send(fighter);
});

router.post(
  "/",
  [...fighterValidationChain, createFighterValid],
  (req, res) => {
    const fighter = FighterService.createFighter(req.body);

    res.send(fighter);
  }
);

router.put(
  "/:id",
  [...fighterValidationChain, createFighterValid],
  (req, res) => {
    const { id } = req.params;

    try {
      const result = FighterService.updateFighter(id, req.body);
      res.send(result);
    } catch (e) {
      res.status(404).send({ error: true, message: e.message });
    }
  }
);

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  try {
    const result = FighterService.deleteFighter(id);
    res.send(result);
  } catch (e) {
    res.status(404).send({ error: true, message: e.message });
  }
});

module.exports = router;

const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");
const { userValidationChain } = require("../models/user.validationChain");

const router = Router();

// TODO: Implement route controllers for user

router.get("/", (req, res) => {
  const allUsers = UserService.getAllUsers();

  res.send(allUsers);
});

router.get("/:id", (req, res) => {
  const { id } = req.params;
  const user = UserService.getUserById(id);

  if (!user) {
    res.status(404).send({ error: true, message: `User isn't found` });
  }

  res.send(user);
});

router.post("/", [...userValidationChain, createUserValid], (req, res) => {
  const user = UserService.createUser(req.body);

  res.send(user);
});

router.put("/:id", [...userValidationChain, createUserValid], (req, res) => {
  const { id } = req.params;

  try {
    const result = UserService.updateUser(id, req.body);
    res.send(result);
  } catch (e) {
    res.status(404).send({ error: true, message: e.message });
  }
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;

  try {
    const result = UserService.deleteUser(id);
    res.send(result);
  } catch (e) {
    res.status(404).send({ error: true, message: e.message });
  }
});

module.exports = router;

class TransformService {
  normalizeErrors(errors) {
    const errorsMsgs = errors.reduce((errorsAcc, { param, msg }) => {
      errorsAcc[param] = msg;
      return errorsAcc;
    }, {});

    return { error: true, errors: errorsMsgs };
  }
}

module.exports = new TransformService();

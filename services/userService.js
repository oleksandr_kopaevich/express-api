const { UserRepository } = require("../repositories/userRepository");
const { filterObject } = require("../services/filterService");

class UserService {
  // TODO: Implement methods to work with user
  constructor() {
    this.authFilter = ["password", "email", "phoneNumber"];
    this.getUserFilter = ["password", "id"];
    this.createUserFilter = [
      "firstName",
      "lastName",
      "email",
      "phoneNumber",
      "password",
    ];
  }

  getUserById(id) {
    const user = UserRepository.getOne({ id });

    if (!user) {
      return null;
    }

    const normalizedUser = filterObject(user, this.getUserFilter, true);

    return normalizedUser;
  }

  search(searchData) {
    const normalizedSearchData = filterObject(searchData, this.authFilter);
    const user = UserRepository.getOne(normalizedSearchData);

    if (!user) {
      return null;
    }

    const normalizedUser = filterObject(user, this.getUserFilter, true);

    return normalizedUser;
  }

  getAllUsers() {
    const users = UserRepository.getAll();
    const normalizedUsers = users.map((user) =>
      filterObject(user, this.getUserFilter, true)
    );
    return normalizedUsers;
  }

  createUser(user) {
    const normalizedUser = filterObject(user, this.createUserFilter);
    const createdUser = UserRepository.create(normalizedUser);
    const normalizedCreatedUser = filterObject(
      createdUser,
      this.getUserFilter,
      true
    );

    return normalizedCreatedUser;
  }

  updateUser(id, userData) {
    const normalizedUser = filterObject(userData, this.createUserFilter);

    return UserRepository.update(id, normalizedUser);
  }

  deleteUser(id) {
    return UserRepository.delete(id);
  }
}

module.exports = new UserService();

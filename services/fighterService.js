const { FighterRepository } = require("../repositories/fighterRepository");
const { filterObject } = require("../services/filterService");

class FighterService {
  constructor() {
    this.createFighterFilter = ["name", "health", "power", "defense"];
    this.searchFilter = ["name"];
  }

  // TODO: Implement methods to work with fighters
  getFighterById(id) {
    const fighter = FighterRepository.getOne({ id });

    if (!fighter) {
      return null;
    }
    return fighter;
  }

  search(searchData) {
    const normalizedSearchData = filterObject(searchData, this.searchFilter);
    const fighter = FighterRepository.getOne(normalizedSearchData);

    if (!fighter) {
      return null;
    }

    return fighter;
  }

  getAllFighters() {
    const fighters = FighterRepository.getAll();
    return fighters;
  }

  createFighter(fighter) {
    const normalizedFighter = filterObject(fighter, this.createFighterFilter);

    return FighterRepository.create(normalizedFighter);
  }

  updateFighter(id, fighter) {
    const normalizedFighter = filterObject(fighter, this.createFighterFilter);

    return FighterRepository.update(id, normalizedFighter);
  }

  deleteFighter(id) {
    return FighterRepository.delete(id);
  }
}

module.exports = new FighterService();

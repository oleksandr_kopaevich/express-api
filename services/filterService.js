class FilterService {
  filterObject(obj, filterArr, isReversed = false) {
    return Object.keys(obj).reduce((prev, current) => {
      if (filterArr.includes(current) !== isReversed) {
        prev[current] = obj[current];
      }

      return prev;
    }, {});
  }
}

module.exports = new FilterService();

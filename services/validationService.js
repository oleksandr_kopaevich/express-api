class ValidationService {
  validateOnlyLetters(value) {
    return /^[^\s!?@#$%^&*()]*$/.test(value);
  }

  validateUAPhoneNumbers(value) {
    return /^((\+380\d{9})|(\d{10}))$/.test(value);
  }

  validateRange(min, max) {
    return (value) => value >= min && value <= max;
  }
}

module.exports = new ValidationService();

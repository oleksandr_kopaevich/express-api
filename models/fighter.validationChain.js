const { body } = require("express-validator");
const {
  validateOnlyLetters,
  validateRange,
} = require("../services/validationService");
const FighterService = require("../services/fighterService");

const fighterValidationChain = [
  body("name")
    .trim()
    .isLength({ min: 1 })
    .withMessage("Fighter's name is very short? Use at least 1 chars")
    .bail()
    .isLength({ max: 20 })
    .withMessage(
      "Sorry, but that name too long. It must be 20 chars or shorter"
    )
    .bail()
    .custom(validateOnlyLetters)
    .withMessage("Name shouldn't contain wrong characters like !?@#$%^&*()")
    .bail()
    .custom((value, { req }) => {
      // We need this chain only when create a new fighter
      if (req.method === "POST") {
        return !Boolean(FighterService.search({ name: value }));
      }

      if (req.method === "PUT") {
        const { id } = req.params;
        const fighterToUpdate = FighterService.getFighterById(id);
        const fighterWithOurNewName = FighterService.search({ name: value });

        if (!fighterWithOurNewName) {
          return true;
        }

        return fighterToUpdate.id === fighterWithOurNewName.id;
      }

      return true;
    })
    .withMessage("Fighter with this name already exists! Try different"),
  body("health")
    .isNumeric()
    .withMessage("Value must be a number")
    .bail()
    .custom(validateRange(45, 450))
    .withMessage("Value should be in range 45-450"),
  body("power")
    .isNumeric()
    .withMessage("Value must be a number")
    .bail()
    .custom(validateRange(0, 10))
    .withMessage("Value should be in range 0-10"),
  body("defense")
    .isNumeric()
    .withMessage("Value must be a number")
    .bail()
    .custom(validateRange(1, 10))
    .withMessage("Value should be in range 1-10"),
];

exports.fighterValidationChain = fighterValidationChain;

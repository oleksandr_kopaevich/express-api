const { body } = require("express-validator");
const {
  validateOnlyLetters,
  validateUAPhoneNumbers,
} = require("../services/validationService");
const UserService = require("../services/userService");

const userValidationChain = [
  body("firstName")
    .trim()
    .optional({ nullable: true, checkFalsy: true })
    .isLength({ min: 2 })
    .withMessage("Is your name really so short? Use at least 2 chars")
    .bail()
    .isLength({ max: 20 })
    .withMessage(
      "Sorry, but your name too long. It must be 20 chars or shorter"
    )
    .bail()
    .custom(validateOnlyLetters)
    .withMessage("Name shouldn't contain wrong characters like !?@#$%^&*()"),
  body("lastName")
    .trim()
    .optional({ nullable: true, checkFalsy: true })
    .isLength({ min: 2 })
    .withMessage("Is your lastname really so short? Use at least 2 chars")
    .bail()
    .isLength({ max: 30 })
    .withMessage(
      "Sorry, but your lastname too long. It must be 30 chars or shorter"
    )
    .bail()
    .custom(validateOnlyLetters)
    .withMessage(
      "Lastname shouldn't contain wrong characters like !?@#$%^&*()"
    ),
  body("email")
    .trim()
    .isLength({ min: 6 })
    .withMessage("Email should be at least 6 chars long")
    .bail()
    .isEmail()
    .withMessage("Please, check your email again, because it's not correct")
    .bail()
    .custom((value, { req }) => {
      // We need this chain only when create a new user

      if (req.method === "POST") {
        return !Boolean(UserService.search({ email: value }));
      }

      if (req.method === "PUT") {
        const { id } = req.params;
        const userToUpdate = UserService.getUserById(id);
        const userWithOurNewEmail = UserService.search({ email: value });

        if (!userWithOurNewEmail) {
          return true;
        }

        return userToUpdate.id === userWithOurNewEmail.id;
      }

      return true;
    })
    .withMessage("This email already in use"),
  body("phoneNumber")
    .custom(validateUAPhoneNumbers)
    .withMessage("Phone number should be in format +380xxxxxxxxx or 0xxxxxxxxx")
    .custom((value, { req }) => {
      // We need this chain only when create a new user
      if (req.method === "POST") {
        !Boolean(UserService.search({ phoneNumber: value }));
      }

      if (req.method === "PUT") {
        const { id } = req.params;
        const userToUpdate = UserService.getUserById(id);
        const userWithOurNewPhone = UserService.search({ phoneNumber: value });

        if (!userWithOurNewPhone) {
          return true;
        }

        return userToUpdate.id === userWithOurNewPhone.id;
      }

      return true;
    })
    .withMessage("This phone number already in use"),
  body("password")
    .isLength({ min: 3 })
    .withMessage(
      "Your password is too weak. Use 3 chars to get at least some protection"
    )
    .bail()
    .isLength({ max: 128 })
    .withMessage(
      "We appreciate your desire to stay protected, but password with more than 128 chars, seriously???"
    ),
];

exports.userValidationChain = userValidationChain;

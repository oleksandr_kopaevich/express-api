const { validationResult } = require("express-validator");
const { user } = require("../models/user");
const { normalizeErrors } = require("../services/transformService");

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const { errors } = validationResult(req);
  const normalizedErrors = normalizeErrors(errors);

  if (errors.length) {
    res.status(400).send(normalizedErrors);
  } else {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

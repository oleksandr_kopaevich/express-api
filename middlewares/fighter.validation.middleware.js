const { validationResult } = require("express-validator");
const { fighter } = require("../models/fighter");
const { normalizeErrors } = require("../services/transformService");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { errors } = validationResult(req);
  const normalizedErrors = normalizeErrors(errors);

  if (errors.length) {
    res.status(400).send(normalizedErrors);
  } else {
    next();
  }
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
